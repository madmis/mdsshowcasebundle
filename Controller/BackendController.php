<?php

namespace Mds\ShowcaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BackendController extends Controller {

    public function indexAction()
    {
        return $this->render('MdsShowcaseBundle:Backend:index.html.twig');
    }
} 