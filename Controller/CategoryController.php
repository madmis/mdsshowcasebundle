<?php

namespace Mds\ShowcaseBundle\Controller;

use Mds\ShowcaseBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CategoryController extends Controller
{
    /**
     * @param Request $Request
     * @param string $permalink
     * @param string $sort
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $Request, $permalink, $sort)
    {
        $Manager = $this->get('ir_category.manager.category');
        /** @var Category $Category */
        $Category = $Manager->findCategoryBy(['permalink' => $permalink]);
        if (!$Category) {
            $this->createNotFoundException();
        }

        $Manager = $this->get('ir_product.manager.product');
        $products = $Manager->repository()->findBy(['category' => $Category], ['price' => $sort]);

        return $this->render('MdsShowcaseBundle:Category:index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @param Request $Request
     * @param Request $MasterRequest
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listCategoriesAction(Request $Request, Request $MasterRequest)
    {
        $Manager = $this->get('ir_category.manager.category');

        return $this->render('MdsShowcaseBundle:Category:_list.html.twig', [
            'categories' => $Manager->getCategoriesHierarchy(),
            'masterRequest' => $MasterRequest,
        ]);
    }

} 