<?php

namespace Mds\ShowcaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @param Request $Request
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $Request, $slug)
    {
        $Manager = $this->get('ir_product.manager.product');
        $Product = $products = $Manager->findProductBy(['slug' => $slug]);
        if (!$Product) {
            $this->createNotFoundException();
        }

        return $this->render('MdsShowcaseBundle:Product:view.html.twig', [
                'product' => $Product,
            ]);
    }
} 