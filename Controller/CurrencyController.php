<?php

namespace Mds\ShowcaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CurrencyController extends Controller
{
    /**
     * @param Request $Request
     * @param $currency
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeCurrencyAction(Request $Request, $currency)
    {
        $this->get('mds_showcase.service.currency_converter')->setCurrency($currency);

        return $this->redirect($Request->headers->get('referer'));
    }
} 