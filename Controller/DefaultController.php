<?php

namespace Mds\ShowcaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Class DefaultController
 * @package Mds\ShowcaseBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $Request
     * @param string $sort
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $Request, $sort)
    {
        $Manager = $this->get('ir_product.manager.product');
        $products = $Manager->repository()->findBy([], ['price' => $sort]);

        return $this->render('MdsShowcaseBundle:Default:index.html.twig', [
                'products' => $products,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContextInterface::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        return $this->render('MdsShowcaseBundle:Default:login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }
}