<?php

namespace Mds\ShowcaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use IR\Bundle\ProductBundle\Model\OptionValue as BaseOptionValue;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_option_value")
 */
class OptionValue extends BaseOptionValue
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}