<?php

namespace Mds\ShowcaseBundle\Entity;

use IR\Bundle\ProductBundle\Doctrine\ProductManager as BaseProductManager;

class ProductManager extends BaseProductManager
{
    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function repository()
    {
        return $this->repository;
    }
} 