<?php

namespace Mds\ShowcaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use IR\Bundle\ProductBundle\Model\ProductOption as BaseProductOption;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="product_products_options",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="product_option_idx", columns={"product_id", "option_id"})}
 * )
 */
class ProductOption extends BaseProductOption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}