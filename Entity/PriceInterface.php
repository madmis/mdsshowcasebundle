<?php

namespace Mds\ShowcaseBundle\Entity;

interface PriceInterface
{
    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price);

    /**
     * @return float
     */
    public function getPrice();
}