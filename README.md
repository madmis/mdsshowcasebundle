MdsShowcaseBundle
=============

This bundle used Informatic Revolution bundles.

Installation
------------

Install MdsShowcaseBundle

``` bash
$ php composer.phar require "mds/showcasebundle": "1.0.*@dev"
```
Install Informatic Revolution bundles

``` bash
$ php composer.phar require "informaticrevolution/category-bundle": "1.0.x-dev"
$ php composer.phar require "informaticrevolution/product-bundle": "1.0.x-dev"
```

Install latest Doctrine bundle

``` bash
$ php composer.phar require "doctrine/doctrine-bundle": "1.3.*@dev"
```

Configuration
------------

Configure routing

``` yaml
# app/config/routing.yml

mds_showcase:
    resource: "@MdsShowcaseBundle/Resources/config/routing.yml"
```

Configure security

``` yaml
# app/config/security.yml

security:
    providers:
        in_memory:
            memory:
                users:
                    admin: { password: admin, roles: 'ROLE_ADMIN' }

    firewalls:
        login_firewall:
            pattern:   ^/login$
            anonymous: ~
        backend:
            pattern:   ^/backend
            anonymous: false
            form_login:
                login_path: login
                check_path: login_check
                csrf_provider: form.csrf_provider
            logout:
                path:   /backend/logout
                target: /
            context: admin
        main:
            pattern:   ^/
            anonymous: ~
            context: admin

    encoders:
        Symfony\Component\Security\Core\User\User: plaintext
```

Configure external bundles

``` yaml
# app/config/config.yml

doctrine:
    ...
    orm:
        ...
        resolve_target_entities:
            IR\Bundle\CategoryBundle\Model\CategoryInterface: Mds\ShowcaseBundle\Entity\Category
            IR\Bundle\ProductBundle\Model\ProductInterface: Mds\ShowcaseBundle\Entity\Product
            IR\Bundle\ProductBundle\Model\OptionInterface: Mds\ShowcaseBundle\Entity\Option


ir_category:
    db_driver: orm
    category_class: Mds\ShowcaseBundle\Entity\Category

ir_product:
    db_driver: orm
    product_class: Mds\ShowcaseBundle\Entity\Product
    option:
        option_class: Mds\ShowcaseBundle\Entity\Option
        option_value_class: Mds\ShowcaseBundle\Entity\OptionValue
        product_option_class: Mds\ShowcaseBundle\Entity\ProductOption

stof_doctrine_extensions:
    orm:
        default:
            tree: true
            sortable: true
            sluggable: true
            timestampable: true
```

Create and update database

    app/console doctrine:database:create
    app/console doctrine:schema:update --force


Override external bundles templates
------------

To override external bundle templates, copy

    Mds/ShowcaseBundle/Resources/extView/*

to

    app/Resources/

