<?php

namespace Mds\ShowcaseBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $rootNode = $builder->root('mds_showcase');

        $rootNode
            ->children()
                ->scalarNode('default_currency_code')
                    ->cannotBeEmpty()
                    ->defaultValue('USD')
                ->end();

        return $builder;
    }
}