<?php

namespace Mds\ShowcaseBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ServiceCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('ir_product.form.type.product');
        $definition->setClass('Mds\ShowcaseBundle\Form\Type\ProductType');

        $definition = $container->getDefinition('ir_product.manager.product.default');
        $definition->setClass('Mds\ShowcaseBundle\Entity\ProductManager');
    }
}