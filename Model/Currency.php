<?php

namespace Mds\ShowcaseBundle\Model;

use Symfony\Component\Intl\Intl;

class Currency implements CurrencyInterface {

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $exchangeRate;

    public function __construct($code, $exchangeRate)
    {
        $this->setCode($code);
        $this->setExchangeRate($exchangeRate);
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Intl::getCurrencyBundle()->getCurrencyName($this->code);
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return Intl::getCurrencyBundle()->getCurrencySymbol($this->code);
    }

    /**
     * @return float
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * @param float $exchangeRate
     * @return $this
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;

        return $this;
    }
}