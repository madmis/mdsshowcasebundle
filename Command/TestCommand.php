<?php

namespace Mds\ShowcaseBundle\Command;

use IR\Bundle\CategoryBundle\Model\CategoryInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mds:showcase:test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    }
}
