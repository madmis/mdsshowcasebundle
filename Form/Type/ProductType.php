<?php

namespace Mds\ShowcaseBundle\Form\Type;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use IR\Bundle\ProductBundle\Form\Type\ProductType as BaseProductType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ProductType
 * @package Mds\ShowcaseBundle\Form\Type
 */
class ProductType extends BaseProductType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('price', 'money', [
                'currency' => 'USD',
                'label' => 'Price',
                'required' => true,
                'empty_data' => 0.0
            ]
        );
        $builder->add('file', 'file', ['label' => 'Image']);
        $builder->add('category', 'entity', [
                'class' => 'MdsShowcaseBundle:Category',
                'query_builder' => function(NestedTreeRepository $repository) {
                        return $repository->getChildrenQueryBuilder(null, false, 'leftNode');
                },
                'property' => 'indentedTitle',
                'required' => true,
            ]
        );
    }
}