<?php

namespace Mds\ShowcaseBundle;

use Mds\ShowcaseBundle\DependencyInjection\Compiler\ServiceCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MdsShowcaseBundle
 * @package Mds\MdsShowcaseBundle
 */
class MdsShowcaseBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ServiceCompilerPass());
    }

}