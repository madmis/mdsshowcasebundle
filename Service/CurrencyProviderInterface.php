<?php

namespace Mds\ShowcaseBundle\Service;

use Mds\ShowcaseBundle\Model\CurrencyInterface;

interface CurrencyProviderInterface
{
    /**
     * @return CurrencyInterface[]
     */
    public function getAvailableCurrencies();

    /**
     * @param $code
     * @return CurrencyInterface
     */
    public function getCurrency($code);

} 