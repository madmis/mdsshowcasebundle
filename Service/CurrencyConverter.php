<?php

namespace Mds\ShowcaseBundle\Service;

use Mds\ShowcaseBundle\Entity\PriceInterface;
use Mds\ShowcaseBundle\Model\CurrencyInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;

class CurrencyConverter implements CurrencyConverterInterface
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var CurrencyProviderInterface
     */
    protected $provider;

    /**
     * The 3-letter ISO 4217 currency code
     * @var string
     */
    protected $defaultCurrency;

    /**
     * @param SessionInterface $Session
     * @param CurrencyProviderInterface $Provider
     * @param string $defaultCurrency The 3-letter ISO 4217 currency code
     */
    public function __construct(SessionInterface $Session, CurrencyProviderInterface $Provider, $defaultCurrency)
    {
        $this->session = $Session;
        $this->provider = $Provider;
        $this->defaultCurrency = $defaultCurrency;
    }


    /**
     * Get default currency
     * @return string
     */
    public function getDefaultCurrency()
    {
        return $this->defaultCurrency;
    }

    /**
     * Get active currency.
     * @return string
     */
    public function getCurrency()
    {
        return $this->session->get('currency', $this->defaultCurrency);
    }

    /**
     * Set active currency.
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->session->set('currency', $currency);
    }

    /**
     * Get list of supported currencies
     * @return CurrencyInterface[]
     */
    public function getCurrencyList()
    {
        return $this->provider->getAvailableCurrencies();
    }

    /**
     * Convert the given amount to equal amount with different currency
     * @param PriceInterface $Price
     * @param string $code new currency code (The 3-letter ISO 4217 currency code)
     * @return float
     */
    public function convert(PriceInterface $Price, $code)
    {
        $Currency = $this->provider->getCurrency($code);
        return bcmul($Price->getPrice(), $Currency->getExchangeRate(), 2);
    }

    /**
     * Convert the given amount to equal amount with different currency and add currency symbol to result
     * @param PriceInterface $Price
     * @param string $code new currency code (The 3-letter ISO 4217 currency code)
     * @return string
     * TODO formatter service
     */
    public function convertToString(PriceInterface $Price, $code)
    {
        $Formatter = new NumberFormatter('en', NumberFormatter::CURRENCY);
        return $Formatter->formatCurrency($this->convert($Price, $code), $code);
    }
}