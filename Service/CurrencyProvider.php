<?php

namespace Mds\ShowcaseBundle\Service;


use Mds\ShowcaseBundle\Model\Currency;
use Mds\ShowcaseBundle\Model\CurrencyInterface;

class CurrencyProvider implements CurrencyProviderInterface
{
    /**
     * @var CurrencyInterface[]
     */
    protected $currencies;

    /**
     * Example:
     *  [
     *      ['code' => USD, 'exchange_rate' => 1],
     *      ['code' => EUR, 'exchange_rate' => 1.3],
     *  ]
     * @param array $currencies array of available currencies
     */
    public function __construct(array $currencies)
    {
        foreach($currencies as $item) {
            $this->currencies[$item['code']] = new Currency($item['code'], $item['exchange_rate']);
        }
    }

    /**
     * @return CurrencyInterface[]
     */
    public function getAvailableCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @param string $code The 3-letter ISO 4217 currency code
     * @return CurrencyInterface
     * @throws \InvalidArgumentException
     */
    public function getCurrency($code)
    {
        if (isset($this->currencies[$code])) {
            return $this->currencies[$code];
        }

        throw new \InvalidArgumentException("Currency {$code} is not supported.");
    }
}