<?php

namespace Mds\ShowcaseBundle\Service;

use Mds\ShowcaseBundle\Entity\PriceInterface;

interface CurrencyConverterInterface
{
    /**
     * Get default currency
     * @return string
     */
    public function getDefaultCurrency();

    /**
     * Get active currency.
     * @return string
     */
    public function getCurrency();

    /**
     * Set active currency.
     * @param string $currency
     */
    public function setCurrency($currency);

    /**
     * Get list of supported currencies
     * @return array
     */
    public function getCurrencyList();

    /**
     * Convert the given amount to equal amount with different currency
     * @param PriceInterface $Price
     * @param string $code new currency code
     * @return integer
     */
    public function convert(PriceInterface $Price, $code);
} 